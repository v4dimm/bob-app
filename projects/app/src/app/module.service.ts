import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  public readonly modules: Observable<Module[]>;
  // modules: any[];

  constructor(readonly httpClient: HttpClient) {
    this.modules = this.httpClient.get<Module[]>('http://127.0.0.1:4202/modules').pipe(shareReplay(1));
  }


  public load({ files, url, selector }: Module, element: HTMLElement): void {
    const entryComponent = document.createElement(selector);
    element.appendChild(entryComponent);

    files.forEach(file => {
        const fileUrl = `http://localhost:4202/module/photos/${file}`; // TODO: use API URL from DI
        const script = document.createElement('script');
        script.type = 'module';
        script.src = fileUrl;
        element.appendChild(script);
      },
    );
  }
}

export interface Module {
  url: string;
  name: string;
  id?: any;
  selector?: string;
  files?: string[];
}

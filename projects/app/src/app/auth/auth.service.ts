import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  login(user: any): Observable<any> {
    if (user.username === 'Bob' && user.password === 'password') {
      user.current = true;
      return of({ token: '2fc1c0beb992cd7096975cfebf9d5c3b' })
        .pipe(
          tap(this.setToken)
        )
    } else {
      const error = {
        message: 'Incorrect username or password'
      }
      return throwError(error);
    }

  }

  logout() {
    this.setToken(null);
  }

  get token(): string {
    const token = localStorage.getItem('fb-token');
    return token;
  }

  setToken(res: any | null) {
    if (res) {
      const expDate = new Date(new Date().getTime() + Math.random() * 1000);
      localStorage.setItem('fb-token', res.token);
      localStorage.setItem('fb-token-exp', expDate.toString());
    } else {
      localStorage.clear();
    }
  }
}

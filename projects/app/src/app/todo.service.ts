import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<Todo[]> {
    return this.http.get<Todo[]>(`${environment.url}/todos`);
  }
}

export interface Todo {
  id: number,
  title: string,
  completed: boolean
}

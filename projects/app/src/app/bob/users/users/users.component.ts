import { Component, OnInit } from '@angular/core';
import { User, UserService } from 'src/app/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: User[];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getAll()
      .subscribe(res => {
        this.users = res;
      })
  }

}

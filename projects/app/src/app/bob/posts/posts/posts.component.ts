import { Component, OnInit } from '@angular/core';
import { Post, PostService } from 'src/app/post.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[];

  constructor(
    private postsService: PostService
  ) { }

  ngOnInit(): void {
    this.postsService.getAll()
      .subscribe(res => {
        this.posts = res.splice(0, 10);
      })
  }

}

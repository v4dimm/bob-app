import { AfterViewInit, Component, ElementRef, Injector, OnInit, TemplateRef, ViewChild, ViewContainerRef, ViewRef } from '@angular/core';
import { Todo, TodoService } from '../../todo.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {

  todos: Todo[];
  @ViewChild('main', {read: ViewContainerRef})
  main: ViewContainerRef

  @ViewChild('tpl') tpl: TemplateRef<any>

  constructor(
    private todoService: TodoService,
    private injector: Injector
    // private host: ViewRef
  ) { }

  ngOnInit(): void {
    this.todoService.getAll()
      .subscribe(res => {
        this.todos = res.slice(0, 20);
      })
  }

  ngAfterViewInit(): void {
    const view = this.tpl.createEmbeddedView(null)
    // console.log(this.main);
    // this.main.insert(view)        
    this.main.clear()
    console.log(this.main);
    
  }
}

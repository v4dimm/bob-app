import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DynamicLayoutComponent } from './dynamic-layout/dynamic-layout.component';

const routes: Routes = [
  { path: '', component: DynamicLayoutComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotosRoutingModule { }

import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, HostListener, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { ModuleService } from 'src/app/module.service';

@Component({
  selector: 'app-dynamic-layout',
  templateUrl: './dynamic-layout.component.html',
  styleUrls: ['./dynamic-layout.component.scss']
})
export class DynamicLayoutComponent implements OnInit {

  private subscription = Subscription.EMPTY;

  constructor(
    private moduleService: ModuleService,
    readonly viewContainerRef: ViewContainerRef,
    private activatedRoute: ActivatedRoute,
    readonly elementRef: ElementRef,
  ) { }

  ngOnInit(): void {
    this.subscription = combineLatest([this.moduleService.modules, this.activatedRoute.params])
      .subscribe(([modules]) => {
        this.viewContainerRef.clear();
        this.moduleService.load(modules[0], this.elementRef.nativeElement);
      });
  }

  @HostListener('document:activate-module', ['$event.detail'])
  public bootstrap(module: { init: (parentContext: any) => void }) {
    // module.init(this.context);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

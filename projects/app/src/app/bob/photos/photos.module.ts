import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicLayoutComponent } from './dynamic-layout/dynamic-layout.component';
import { PhotosRoutingModule } from './photos-routing.module';

@NgModule({
  declarations: [DynamicLayoutComponent],
  imports: [
    CommonModule,
    PhotosRoutingModule
  ]
})
export class PhotosModule { }

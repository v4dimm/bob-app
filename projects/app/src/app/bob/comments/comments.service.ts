import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${environment.url}/comments`);
  }
}

export interface Comment {
  id: number,
  name: string,
  body: string
}

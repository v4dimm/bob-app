import { Component, OnInit } from '@angular/core';
import { CommentsService, Comment } from '../comments.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  comments: Comment[];

  constructor(
    private commentsService: CommentsService
  ) { }

  ngOnInit(): void {
    this.commentsService.getAll()
      .subscribe(res => {
        this.comments = res.splice(0, 15);
      })
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { MainComponent } from './bob/main/main.component';
import { LayoutComponent } from './shared/layout/layout.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      { path: '', redirectTo: '/main', pathMatch: 'full' },

      // Component from Root Module
      { path: 'main', component: MainComponent },

      //Lazy Load module without CanLoad
      { path: 'posts', loadChildren: () => import('./bob/posts/posts.module').then(m => m.PostsModule) },

      //Lazy Load module with CanLoad
      { path: 'users', canLoad: [AuthGuard], loadChildren: () => import('./bob/users/users.module').then(m => m.UsersModule) },

      //Load Module from server
      { path: 'comments', loadChildren: () => import('./bob/comments/comments.module').then(m => m.CommentsModule) },

      //Use WebComponent
      { path: 'photos', canLoad: [AuthGuard], loadChildren: () => import('./bob/photos/photos.module').then(m => m.PhotosModule) },
    ]
  },
  { path: 'login', component: LoginPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

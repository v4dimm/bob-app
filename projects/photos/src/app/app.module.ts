import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';
import { PhotoComponent } from './photo/photo.component';
import { createCustomElement } from '@angular/elements';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    PhotoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: [PhotoComponent]
})
export class AppModule {

  constructor(injector: Injector) {
    const photos = createCustomElement(PhotoComponent, { injector })
    customElements.define('app-photos', photos)
  }

  ngDoBootstrap() {
  }
}

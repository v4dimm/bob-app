import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<Photo[]> {
    return this.http.get<Photo[]>(`${environment.url}/photos`);
  }
}

export interface Photo {
  id: number,
  title: string,
  url: string
}

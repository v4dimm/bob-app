import { Component, OnInit } from '@angular/core';
import { Photo, PhotosService } from '../photos.service';


@Component({
    selector: 'app-photos',
    templateUrl: './photo.component.html',
    styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {

    photos: Photo[];

    constructor(
        private photosService: PhotosService
    ) { }

    ngOnInit(): void {
        this.photosService.getAll()
            .subscribe(res => {
                this.photos = res.splice(0, 3);
            })
    }
}

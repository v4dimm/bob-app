### STAGE 1: Build ###
FROM node:14.8.0 as node_cache
WORKDIR /cache

COPY package.json .
RUN npm set registry http://registry.npmjs.org
RUN npm i && npm i http-server

FROM node:14.8.0 as node_build
WORKDIR /app
COPY --from=node_cache /cache/ .
COPY . .
RUN npm run-script build --prod


### STAGE 2: Setup ###
FROM nginx:stable-alpine

## Copy our default nginx config
COPY config/default.conf /etc/nginx/conf.d/default.conf
COPY --from=node_build /app/dist/bob-app /usr/share/nginx/html
#CMD ["nginx", "-g", "daemon off;"]

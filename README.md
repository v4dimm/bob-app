# Bob App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Build

Run `docker build -t bob-app .` to build the docker image. Then run `docker run -p 8080:80 bob-app` .
